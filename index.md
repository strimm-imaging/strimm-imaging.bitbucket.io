---
layout: default
---

# {{site.data.package.name}}
![image]({{site.data.package.logo}})

{{site.data.package.abstract}}
{{site.data.package.abstract2}}
<!-- The current version of this package is version {{site.data.package.version}}, released on {{site.data.package.date}}. -->
<!-- For more information, please refer to [the package manual]({{site.data.package.doc-html}}). -->
<!--There is also a [README](README.html) file.-->

## Dependencies

{% if site.data.package.needed-pkgs %}
The following other software packages are needed:
{% for pkg in site.data.package.needed-pkgs %}
- {% if pkg.url %}<a href="{{ pkg.url }}">{{ pkg.name }}</a> {% else %}{{ pkg.name }} {% endif %}
  {{- pkg.version -}}
{% endfor %}
{% endif %}

## Author{% if site.data.package.authors.size != 1 %}s{% endif %}
{% for person in site.data.package.authors %}
 {% if person.url %}<a href="{{ person.url }}">{{ person.name }}</a>{% else %}{{ person.name }}{% endif %}
 {%- if forloop.last -%}.{% else %}, {%- endif -%}
{% endfor %}

{% if site.data.package.contributors and site.data.package.contributors.size > 0 %}
## Contributor{% if site.data.package.contributors.size != 1 %}s{% endif %}
 {% for person in site.data.package.contributors %}
  {% if person.url %}<a href="{{ person.url }}">{{ person.name }}, {{ person.organisation }}</a>{% else %}{{ person.name }}, {{ person.organisation }}<br>{% endif %}
  <!--{%- if forloop.last -%}.{% else %}, {%- endif -%}-->
 {% endfor %}
{% endif %}

## License
{% for pkg in site.data.package.license %}
{% if pkg.url %}<a href="{{ pkg.url }}">{{ pkg.type }}</a> {% else %}{{ pkg.type }} {% endif %}
{% endfor %}

{% if site.github.issues_url %}
## Feedback

For bug reports, feature requests and suggestions, please use the
[issue tracker]({{site.github.issues_url}}).
{% endif %}

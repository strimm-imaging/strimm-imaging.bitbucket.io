# STRÏMM website maintenance HOWTO

This document describes maintaining the STRÏMM website
<https://strimm-imaging.bitbucket.io/>.

It uses the the static web site generator called [Jekyll](https://jekyllrb.com/).
Its setup and parts of this HOWTO file are based on the setup used by
[GitHubPagesForGAP](https://github.com/gap-system/GitHubPagesForGAP) by Max Horn,
adapted for hosting on the Bitbucket Cloud as explained at
<https://support.atlassian.com/bitbucket-cloud/docs/publishing-a-website-on-bitbucket-cloud/>.

This website is contained in the following repository:
https://bitbucket.org/strimm-imaging/strimm-imaging.bitbucket.io.git

It has two branches:

- `website-source`: the source of the website

- `master`        : the generated content to be published 


The basic workflow is as follows:

1. make changes in the `website-source` branch

2. build and test the local version of the website

3. commit changes in the `website-source` branch

4. copy generated content to the `master` branch

5. commit changes in the `master` branch

If you are familiar with [GitHub Pages](https://pages.github.com/),
note that the Bitbucket Cloud requires slightly more complex setup:
firstly, the Bitbucket Cloud requires the content of the website to
be kept in the master branch; secondly, it does not Jekyll to generate
the content on the host server, and hence we have to keep the generated
content under version control.


## Setting up

The following instructions assume that you have cloned this repository
into the directory `strimm-imaging.bitbucket.io`.

1. Go into the directory `strimm-imaging.bitbucket.io`.

2. Establish a clone of the `website-source` branch of the repository
in this directory.

Users with a recent enough git version (recommended is >= 2.7)
can do this using a "worktree", via the following commands:
```
git branch website-source origin/website-source
git worktree add website-source website-source
```

If you are using an older version of git, you can instead use a second clone
of your repository instead:
```
git clone -b website-source https://bitbucket.org/strimm-imaging/strimm-imaging.bitbucket.io.git website-source
```

## Testing the site locally

To test your site on your own machine, you need to install the static
web site generator called [Jekyll](http://jekyllrb.com/). Once you have
installed Jekyll as described on its homepage, you can test the website
locally as follows:

1. Go to the `website-source` directory we created above.

2. Run jekyll (this launches a tiny web server on your machine):
    ```
    jekyll serve -w
    ```

3. Visit the URL <http://localhost:4000> in a web browser.


## Publishing website updates

Whenever you want to update the website, you need to follow the following
steps

1. Go to the `website-source` directory we created above

2. Make necessary changes in the content and layout.

3. Build and serve the local version of the website:

    ```
    jekyll serve -w
    ```
    
4. Visit the URL http://localhost:4000 in a web browser to test the website.

5. When you're happy with the changes, commit them in the `website-source` branch

6. Copy generated content to the `master` branch: assuming that you are in the directory
   `strimm-imaging.bitbucket.io/website-source`, call

    ```
    cp -r _site/ ../
    ```
   to copy the generated content to the clone of the `master` branch
   
7. Navigate to the directory `strimm-imaging.bitbucket.io`, commit and push changes, e.g.:

```
cd ..
git add *
git commit -m "Update website"
git push
```

A few seconds after you have done this, your changes will be online
at https://strimm-imaging.bitbucket.io/. However, the Bitbucket Cloud
documentation states that the old pages may be cached for up to 15
minutes, so you may experience a delay in update.

## Adjusting the content and layout

This setup offers sufficiently good defaults for a software package.
However, if need be, you can tweak everything about it:

* To adjust the software metadata for the front page, edit the file
`_data/package.yml`.

* To adjust the page layout, edit the files `stylesheets/styles.css`
and `_layouts/default.html`.

* To adjust the content of the front page, edit `index.md` (resp.
  for the content of the sidebar, edit `_layouts/default.html`

* You can also add additional pages, in various formats (HTML,
Markdown, Textile, ...).

For details, please consult the [Jekyll](http://jekyllrb.com/)
manual.
